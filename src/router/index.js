import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/LandingPage.vue";
import Login from "../views/Login.vue";
import Signup from "../views/Signup.vue";
import GuestLogin from "../views/GuestLogin.vue";
import StudentHome from "../views/StudentHome.vue";
import MarketingHome from "../views/MarketingHome.vue";
import CoordinatorHome from "../views/CoordinatorHome.vue";
import CreatePost from "../components/createPostForm.vue";
import EditPost from "../components/editpost.vue";
import Manage from "../components/manage.vue";
import FacultyEdit from "../components/facultyEdit.vue";
import ViewPost from "../components/viewPost.vue";
import ViewAllPost from "../components/ViewAllPost.vue";
import ViewFacultyPost from "../components/ViewFacultyPost";
import FacultyPost from "../components/FacultyPost.vue";
import AllPost from "../components/AllPost.vue";
import Post from "../components/Post.vue";
import Faculty from "../views/Reports.vue";
import Reports from "../components/reports.vue";
import * as firebase from "firebase/app";
import "firebase/auth";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    meta: { Auth: false },
    component: Home,
  },
  {
    path: "/Login",
    name: "Login",
    meta: { Auth: false },
    component: Login,
  },
  {
    path: "/GuestLogin",
    name: "GuestLogin",
    component: GuestLogin,
    meta: { Auth: false },
  },
  {
    path: "/:facultyID",
    name: "Faculty",
    component: Faculty,
    props: true,
    children: [
      {
        path: "Reports",
        name: "Reports",
        component: Reports,
        meta: { Auth: true },
        props: true,
      },
    ],
  },
  {
    path: "/Signup",
    name: "Signup",
    meta: { Auth: false },
    component: Signup,
  },
  {
    path: "/MarketingHome",
    name: "MarketingHome",
    component: MarketingHome,
    meta: { Auth: true },
    props: true,
    children: [
      {
        path: "AllPost",
        name: "AllPost",
        component: AllPost,
        props: true,
      },
      {
        path: ":post_id",
        name: "ViewAllPost",
        component: ViewAllPost,
        props: true,
      },
      {
        path: "Manage",
        name: "Manage",
        component: Manage,
        props: true,
      },
    ],
  },
  {
    path: "/CoordinatorHome",
    name: "CoordinatorHome",
    component: CoordinatorHome,
    meta: { Auth: true },
    children: [
      {
        path: "FacultyPost",
        name: "FacultyPost",
        component: FacultyPost,
        props: true,
      },
      {
        path: ":post_id",
        name: "ViewFacultyPost",
        component: ViewFacultyPost,
        props: true,
      },
      {
        path: "FacultyEdit",
        name: "FacultyEdit",
        component: FacultyEdit,
        props: true,
      },
    ],
  },
  {
    path: "/StudentHome",
    name: "StudentHome",
    component: StudentHome,
    meta: { Auth: true },
    props: true,
    children: [
      {
        path: "CreatePost",
        name: "CreatePost",
        component: CreatePost,
        props: true,
      },
      {
        path: "Post",
        name: "Post",
        component: Post,
        props: true,
      },
      {
        path: "EditPost",
        name: "EditPost",
        component: EditPost,
        props: true,
      },
      {
        path: ":post_id",
        name: "ViewPost",
        component: ViewPost,
        props: true,
      },
    ],
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  const Auth = to.matched.some((record) => record.meta.Auth);
  const Authenticated = firebase.auth().currentUser;
  if (Auth && !Authenticated) {
    next("/login");
  } else {
    next();
  }
});

export default router;
